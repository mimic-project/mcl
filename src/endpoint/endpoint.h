//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_ENDPOINT_ENDPOINT_H_
#define MCL_ENDPOINT_ENDPOINT_H_

#include <iostream>
#include <memory>

#include "error_handler.h"
#include "transport.h"

namespace mcl {

/// Endpoint class representing clients involved in data communications
class Endpoint {

protected:

    /// Transport protocol used to send data
    std::shared_ptr<Transport> protocol;

    /// List of known clients
    std::vector<std::unique_ptr<Endpoint>> client_list;

    /// Endpoint id
    int id;

    /// Internal tag to identify messages containing client ID
    static constexpr int CLIENT_ID_TAG = 410;


public:

    Endpoint(std::shared_ptr<Transport> protocol) : protocol(protocol) {}

    Endpoint(std::shared_ptr<Transport> protocol, int id) : protocol(protocol), id(id) {}

    virtual ~Endpoint() {}

    /// Send data to a specific client (transport layer should handle data packaging)
    /**
     * \param data pointer to the data array
     * \param count number of data entities
     * \param destination id of the client to receive data
     * \param data_type of the data to send
     * \param tag message tag that used for matching
     */
    int send(void *data, int count, int destination, int data_type, int tag) {
        bool found = false;
        for (auto & client : client_list) {
            if (client->getId() == destination) {
                protocol->send(data, count, data_type, tag, destination);
                found = true;
                break;
            }
        }

        if (!found) {
            handle_error(ERROR_FATAL, "Send: Unknown destination is chosen!", EARGS);
        }
        return 0;
    }

    /// Receive raw data (array of primitive data_type) from a specific client
    /**
     * \param data pointer to the buffer
     * \param count number of data entities
     * \param source id of the client to receive data
     * \param data_type of the data to send
     * \param tag message tag that used for matching
     */
    void receive(void *data, int count, int source, int data_type, int tag) {
        bool found = false;
        for (auto & client : client_list) {
            if (client->getId() == source) {
                protocol->receive(data, count, data_type, tag, source);
                found = true;
                break;
            }
        }

        if (!found) {
            handle_error(ERROR_FATAL, "Receive: Unknown source is chosen!", EARGS);
        }
    }

    /// Probe message queue for length of the pending message
    /**
     * \param client id of the client
     * \param data_type of the data to check
     */
    int probe(int client, int data_type) {
        return protocol->probe(client, data_type);
    }

    /// Disconnect from a specified client
    virtual void disconnect(int dest) = 0;

    /// Destroy the endpoint
    virtual void destroy() = 0;

    /// Returns an ID of the client
    /**
     * \return id of the client
     */
    int getId() const {
        return id;
    }

    void setId(int id) {
        Endpoint::id = id;
    }

    const std::vector<std::unique_ptr<Endpoint>> &getClient_list() const {
        return client_list;
    }

};

}

#endif // MCL_ENDPOINT_ENDPOINT_H_
