//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mcl_c.h"

#include "mpi.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#if defined(_WIN32) || defined(_WIN64)
#define ADD_ENV_VAR(NAME, VALUE) _putenv(NAME "=" VALUE)
#else
#define ADD_ENV_VAR(NAME, VALUE) setenv(NAME, VALUE, 1)
#endif

using ::testing::TestEventListeners;
using ::testing::UnitTest;
using ::testing::ElementsAreArray;
using ::testing::Not;
using ::testing::ContainsRegex;
using ::testing::MatchesRegex;
using ::testing::internal::CaptureStderr;
using ::testing::internal::GetCapturedStderr;

TEST(APICppTest, MimicLoop) {
    MPI_Comm localComm = MPI_COMM_WORLD;
    MCL_init(&localComm);
    
    int commRank;
    MPI_Comm_rank(localComm, &commRank); 
    int commSize;
    MPI_Comm_size(localComm, &commSize);

    int programId = MCL_Get_program_id();
    int numPrograms = MCL_Get_program_num();
    ASSERT_EQ(numPrograms, 3);

    int    refNumAtoms[2] = {3, 2};
    double refEnergy[2] = {0.410, 2.56};
    std::vector<std::vector<double>> refCoords;
    refCoords.push_back({ 1.0,  2.0,  3.0,  4.0,  5.0,  6.0, 7.0, 8.0, 9.0});
    refCoords.push_back({-1.0, -2.0, -3.0, -4.0, -5.0, -6.0});

    // server
    if (programId == 0) {
        ASSERT_EQ(commSize, 3);
        int request;

        // MCL_SEND_CLIENT_ID
        for (size_t i = 1; i <= 2; i++) {
            request = MCL_SEND_CLIENT_ID;
            MCL_send(&request, 1, MCL_COMMAND, i);
            int clientId = -1;
            MCL_receive(&clientId, 1, MCL_DATA, i);
            ASSERT_TRUE((clientId == (int) i) == (commRank == 0));
            MPI_Bcast(&clientId, 1, MPI_INT, 0, localComm);
            ASSERT_EQ(clientId, i);
        }

        int numAtoms[2] = {-1, -1};
        // MCL_SEND_NUM_ATOMS
        for (size_t i = 1; i <= 2; i++) {
            request = MCL_SEND_NUM_ATOMS;
            MCL_send(&request, 1, MCL_COMMAND, i);
            MCL_receive(&(numAtoms[i-1]), 1, MCL_DATA, i);
            ASSERT_TRUE((numAtoms[i-1] == refNumAtoms[i-1]) == (commRank == 0));
            MPI_Bcast(&(numAtoms[i-1]), 1, MPI_INT, 0, localComm);
            ASSERT_EQ(numAtoms[i-1], refNumAtoms[i-1]);
        }

        // MCL_RECV_ATOM_COORDS
        for (size_t i = 1; i <= 2; i++) {
            request = MCL_RECV_ATOM_COORDS;
            MCL_send(&request, 1, MCL_COMMAND, i);
            MCL_send(refCoords[i-1].data(), 3*numAtoms[i-1], MCL_DATA, i);
        }

        // MCL_COMPUTE_ENERGY
        for (size_t i = 1; i <= 2; i++) {
            request = MCL_COMPUTE_ENERGY;
            MCL_send(&request, 1, MCL_COMMAND, i);
        }

        // MCL_SEND_ENERGY
        for (size_t i = 1; i <= 2; i++) {
            request = MCL_SEND_ENERGY;
            MCL_send(&request, 1, MCL_COMMAND, i);
            double energy = 0.0;
            MCL_receive(&energy, 1, MCL_DATA, i);
            ASSERT_TRUE((energy == refEnergy[i - 1]) == (commRank == 0));
            MPI_Bcast(&energy, 1, MPI_DOUBLE, 0, localComm);
            ASSERT_EQ(energy, refEnergy[i - 1]);
        }

        // MCL_EXIT
        request = MCL_EXIT;
        MCL_send(&request, 1, MCL_COMMAND, 1);
        MCL_send(&request, 1, MCL_COMMAND, 2);
    }

    // clients
    if (programId == 1 || programId == 2) {
        ASSERT_EQ(commSize, 2);
        
        int numAtoms = refNumAtoms[programId-1];

        bool computed_energy = false;
        bool terminate = false;
        while (!terminate) {
            int request = -1;
            MCL_receive(&request, 1, MCL_COMMAND, 0);
            MPI_Bcast(&request, 1, MPI_INT, 0, localComm);

            if (request == MCL_SEND_CLIENT_ID) {
                MCL_send(&programId, 1, MCL_DATA, 0);
            } else if (request == MCL_SEND_NUM_ATOMS) {
                MCL_send(&numAtoms, 1, MCL_DATA, 0);
            } else if (request == MCL_RECV_ATOM_COORDS) {
                std::vector<double> coords(3*numAtoms, 0.0);
                MCL_receive(coords.data(), 3*numAtoms, MCL_DATA, 0);
                MPI_Bcast(coords.data(), 3*numAtoms, MPI_DOUBLE, 0, localComm);
                ASSERT_THAT(coords, ElementsAreArray(refCoords[programId-1]));
            } else if (request == MCL_COMPUTE_ENERGY) {
                computed_energy = true;
            } else if (request == MCL_SEND_ENERGY) {
                ASSERT_TRUE(computed_energy);
                double energy = refEnergy[programId - 1];
                MCL_send(&energy, 1, MCL_DATA, 0);
            } else if (request == MCL_EXIT) {
                terminate = true;
            } else {
                std::cerr << "ABORT: Unknown MCL request!" << std::endl;
                std::abort();
            }
        }
    }

    MCL_destroy();
}

TEST(APICppTest, DataTypes) {
    MPI_Comm local_comm = MPI_COMM_WORLD;
    MCL_init(&local_comm);
    int program_id = MCL_Get_program_id();
    
    int length = 5;

    std::string ref_chars = "Test!";
    int ref_ints[] = {1, 2, 3, 4, 5};
    long int ref_longints[] = {1, 2, 3, 4, 5};
    float ref_floats[] = {1.0, 2.0, 3.0, 4.0, 5.0};
    double ref_doubles[] = {1.0, 2.0, 3.0, 4.0, 5.0}; 


    if (program_id == 0) {
        MCL_send(ref_chars.data(), length, MCL_DATA, 1);
        MCL_send(ref_ints, length, MCL_DATA, 1);
        MCL_send(ref_longints, length, MCL_DATA, 1);
        MCL_send(ref_floats, length, MCL_DATA, 1);
        MCL_send(ref_doubles, length, MCL_DATA, 1);
    }

    if (program_id == 1) {
        std::string chars;
        chars.resize(length);
        MCL_receive(chars.data(), length, MCL_DATA, 0);
        MPI_Bcast(chars.data(), length, MPI_CHAR, 0, local_comm);
        ASSERT_THAT(chars, MatchesRegex(ref_chars));

        int ints[5];
        MCL_receive(ints, length, MCL_DATA, 0);
        MPI_Bcast(ints, length, MPI_INT, 0, local_comm);
        ASSERT_THAT(ints, ElementsAreArray(ref_ints));

        long int longints[5];
        MCL_receive(longints, length, MCL_DATA, 0);
        MPI_Bcast(longints, length, MPI_LONG, 0, local_comm);
        ASSERT_THAT(longints, ElementsAreArray(ref_longints));
        
        float floats[5];
        MCL_receive(floats, length, MCL_DATA, 0);
        MPI_Bcast(floats, length, MPI_FLOAT, 0, local_comm);
        ASSERT_THAT(floats, ElementsAreArray(ref_floats));
        
        double doubles[5];
        MCL_receive(doubles, length, MCL_DATA, 0);
        MPI_Bcast(doubles, length, MPI_DOUBLE, 0, local_comm);
        ASSERT_THAT(doubles, ElementsAreArray(ref_doubles));
    }
}

TEST(APICppTest, MPICommMode) {
    if (ADD_ENV_VAR("MCL_COMM", std::to_string(1).c_str()) != 0)
        throw std::runtime_error("Could_not_set_env_var!");
    MPI_Comm localComm = MPI_COMM_WORLD;
    std::string error_msg = "The environment variable, MCL_PROG is not assigned!";
    ASSERT_DEATH(MCL_init(&localComm), ContainsRegex(error_msg));
}

TEST(APICppTest, MPMDCommMode) {
    if (ADD_ENV_VAR("MCL_COMM", std::to_string(2).c_str()) != 0)
        throw std::runtime_error("Could_not_set_env_var!");
    MPI_Comm localComm = MPI_COMM_WORLD;
    int result = MCL_init(&localComm);
    ASSERT_EQ(result, 0);
    int numPrograms = MCL_Get_program_num();
    ASSERT_EQ(numPrograms, 3);
    MCL_destroy();
}

TEST(APICppTest, DefaultCommMode) {
    if (ADD_ENV_VAR("MCL_COMM", std::to_string(-1).c_str()) != 0)
        throw std::runtime_error("Could_not_set_env_var!");
    MPI_Comm localComm = MPI_COMM_WORLD;
    CaptureStderr();
    int result = MCL_init(&localComm);
    ASSERT_EQ(result, 0);
    std::string error = GetCapturedStderr();
    std::string error_msg = "Unknown communication mechanism was chosen!";
    ASSERT_THAT(error, ContainsRegex(error_msg));
    int numPrograms = MCL_Get_program_num();
    ASSERT_EQ(numPrograms, 3);
    MCL_destroy();
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    MPI_Init(&argc, &argv);
    
    int worldRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);
    TestEventListeners& listeners = UnitTest::GetInstance()->listeners();
    if (worldRank != 0) 
        delete listeners.Release(listeners.default_result_printer());

    auto result = RUN_ALL_TESTS();
    
    MPI_Finalize();
    return result;
}
