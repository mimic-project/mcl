//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mcl_c.h"

#include "mpi.h"

#include "data_types.h"
#include "env_options.h"
#include "error_handler.h"
#include "main_class.h"
#include "transport/mpi_transport.h"
#include "transport/mpmd_transport.h"
#include "utils.h"

using namespace mcl;

int MCL_init(void *param) {

    // get the env variable
    int comm_key = get_env_var_int(COMMUNICATION_KEY, MCL_TR_MPMD);
    
    // decide which type of Comm. is desired an initialize
    switch (comm_key) {
        case MCL_TR_MPI:
            MCLMain::getInstance()
                    .setProtocol(std::make_shared<MPITransport>(*static_cast<MPI_Comm *>(param)));
            break;
        case MCL_TR_MPMD:
            MCLMain::getInstance()
                    .setProtocol(std::make_shared<MPMDTransport>(*static_cast<MPI_Comm *>(param)));
            break;
        default:
            handle_error(ERROR_WARNING, "Unknown communication mechanism was chosen! "
                                        "MCL_TR_MPMD will be used!", EARGS);
            MCLMain::getInstance()
                    .setProtocol(std::make_shared<MPMDTransport>(*static_cast<MPI_Comm *>(param)));
    }

    // initialize
    MCLMain::getInstance().initialize(param);
    
    return 0;
}

void MCL_destroy() {
    MCLMain::getInstance().destroy();
}

int MCL_Get_program_num() {
    return MCLMain::getInstance().getProgramNum();
}

int MCL_Get_program_id() {
    return MCLMain::getInstance().getProgramId();
}

void MCL_send(void* buffer, int length, int data_type, int tag, int destination) {
    MCLMain::getInstance().send(buffer, length, data_type, tag, destination);
}
void MCL_send(char* buffer, int length, int tag, int destination) {
    MCL_send(buffer, length, TYPE_CHAR, tag, destination);
}
void MCL_send(int* buffer, int length, int tag, int destination) {
    MCL_send(buffer, length, TYPE_INT, tag, destination);
}
void MCL_send(long int* buffer, int length, int tag, int destination) {
    MCL_send(buffer, length, TYPE_LONG_INT, tag, destination);
}
void MCL_send(float* buffer, int length, int tag, int destination) {
    MCL_send(buffer, length, TYPE_FLOAT, tag, destination);
}
void MCL_send(double* buffer, int length, int tag, int destination) {
    MCL_send(buffer, length, TYPE_DOUBLE, tag, destination);
}

void MCL_receive(void* buffer, int length, int data_type, int tag, int source) {
    MCLMain::getInstance().receive(buffer, length, data_type, tag, source);
}
void MCL_receive(char* buffer, int length, int tag, int source) {
    MCL_receive(buffer, length, TYPE_CHAR, tag, source);
}
void MCL_receive(int* buffer, int length, int tag, int source) {
    MCL_receive(buffer, length, TYPE_INT, tag, source);
}
void MCL_receive(long int* buffer, int length, int tag, int source) {
    MCL_receive(buffer, length, TYPE_LONG_INT, tag, source);
}
void MCL_receive(float* buffer, int length, int tag, int source) {
    MCL_receive(buffer, length, TYPE_FLOAT, tag, source);
}
void MCL_receive(double* buffer, int length, int tag, int source) {
    MCL_receive(buffer, length, TYPE_DOUBLE, tag, source);
}

const int MCL_CHAR      = TYPE_CHAR;
const int MCL_INT       = TYPE_INT;
const int MCL_LONG_INT  = TYPE_LONG_INT;
const int MCL_FLOAT     = TYPE_FLOAT;
const int MCL_DOUBLE    = TYPE_DOUBLE;
