//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "error_handler.h"

#include <csignal>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "mcl_config.h"

using namespace mcl;
using ::testing::StrEq;
using ::testing::ContainsRegex;
using ::testing::internal::CaptureStderr;
using ::testing::internal::GetCapturedStderr;

static bool test_handler_called = false;
static constexpr char INDENT[]   = " ";
static constexpr int  LINE_WIDTH = 85;

TEST(DefaultHandler, Warning) {
    std::stringstream ref;

    ref << std::string(LINE_WIDTH, '~') << std::endl;
    ref << INDENT << "Program:      " << PROJECT_NAME << " v" << PROJECT_VER << std::endl;
    ref << INDENT << "Source file:  " << __FILE__ << std::endl;
    ref << INDENT << "Line:         " << __LINE__ + 6 << std::endl;
    ref << std::endl;
    ref << INDENT << "Test warning" << std::endl;
    ref << std::string(LINE_WIDTH, '~') << std::endl;
    
    CaptureStderr();
    handle_error(ERROR_WARNING, "Test warning", EARGS);
    std::string error = GetCapturedStderr();
    
    ASSERT_THAT(ref.str(), StrEq(error));
}

TEST(DefaultHandler, Fatal) {
    std::string error_msg = "Test fatal";
    ASSERT_DEATH(handle_error(ERROR_FATAL, error_msg, EARGS), ContainsRegex(error_msg));
}

void CustomHandlerWarning(const int type, const std::string& msg, const char* file, int line) {
    test_handler_called = true;
}

TEST(CustomHandler, Warning) {
    set_error_handler(CustomHandlerWarning);
    handle_error(ERROR_WARNING, "Test warning", EARGS);
    ASSERT_TRUE(test_handler_called);
    test_handler_called = false;
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    auto result = RUN_ALL_TESTS();
    return result;
}
