//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "main_class.h"

#include <memory>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "data_types.h"

using namespace mcl;
using ::testing::Exactly;
using ::testing::AtMost;
using ::testing::_;
using ::testing::Return;
using ::testing::ContainsRegex;

class MockTransport : public Transport {
public:
    MockTransport() : Transport() {}

    ~MockTransport() override = default;

    MOCK_METHOD(int,  initialize,       (void *args));
    MOCK_METHOD(int,  connect,          (int id));
    MOCK_METHOD(int,  acceptConnection, (int id));
    MOCK_METHOD(void, closeConnection,  (int id));
    MOCK_METHOD(int,  probe,            (int id, int data_type));
    MOCK_METHOD(void, destroy,          (int id));
    MOCK_METHOD(void, send,             (void* data, int count, int data_type, int tag, int id));
    MOCK_METHOD(void, receive,          (void* data_holder, int count, int data_type, int tag, int source));
    MOCK_METHOD(bool, isServer,         (), (const));
    MOCK_METHOD(bool, isClient,         (), (const));
    MOCK_METHOD(int,  getNPrograms,     (), (const));
    MOCK_METHOD(int,  getProgramId,     (), (const));
};

static constexpr int SEND_TAG = 16;
static constexpr int RECEIVE_TAG = 17;

class ServerTest : public ::testing::Test {
  protected:
    std::shared_ptr<MockTransport> protocol = std::make_shared<MockTransport>();
    size_t num_program = 5;
    bool is_server = true;
};

void ExpectEndpointTypeInquiry(std::shared_ptr<MockTransport>& protocol, bool is_server, int max_times) {
    EXPECT_CALL(*protocol, isServer()).Times(AtMost(max_times)).WillOnce(Return(is_server));
    EXPECT_CALL(*protocol, isClient()).Times(AtMost(max_times)).WillOnce(Return(!is_server));
}

void InitializeMainServer(MCLMain& main, std::shared_ptr<MockTransport>& protocol, int num_program) {
    main.setProtocol(protocol);
    EXPECT_CALL(*protocol, initialize(nullptr)).Times(Exactly(1));
    ExpectEndpointTypeInquiry(protocol, true, 1);
    int client_id = 1;
    EXPECT_CALL(*protocol, getNPrograms()).Times(Exactly(2)).WillRepeatedly(Return(num_program));
    for (size_t i = 1; i < (size_t) num_program; i++) {
        EXPECT_CALL(*protocol, acceptConnection(i)).Times(Exactly(1));
        EXPECT_CALL(*protocol, receive(_, 1, TYPE_INT, _, i))
                                .Times(Exactly(1))
                                .WillOnce([&](void* data, int count, int data_type, int tag, int source) {
                                            *((int*) data) = client_id++;
                                          }
                                         );
    }
    main.initialize(nullptr);
}

TEST_F(ServerTest, Initialize) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainServer(main, protocol, num_program);

    ASSERT_EQ(main.getEndpoint()->getId(), 0);
    ASSERT_EQ(main.getEndpoint()->getClient_list().size(), num_program-1);
    for (size_t i = 0; i < main.getEndpoint()->getClient_list().size(); ++i) {
        ASSERT_EQ(i+1, main.getEndpoint()->getClient_list()[i]->getId());
    }
}

TEST_F(ServerTest, Receive) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainServer(main, protocol, num_program);
    
    int testData[5];
    ExpectEndpointTypeInquiry(protocol, is_server, num_program-1);
    for (size_t i = 0; i < main.getEndpoint()->getClient_list().size(); ++i) {
        EXPECT_CALL(*protocol, receive(testData, 5, TYPE_INT, RECEIVE_TAG, i+1)).Times(Exactly(1));
    }
    for (size_t i = 0; i < main.getEndpoint()->getClient_list().size(); ++i) {
        main.receive(testData, 5, TYPE_INT, RECEIVE_TAG, i+1);
    }
}

TEST_F(ServerTest, Send) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainServer(main, protocol, num_program);

    int testData[5];
    ExpectEndpointTypeInquiry(protocol, is_server, num_program-1);
    for (size_t i = 0; i < main.getEndpoint()->getClient_list().size(); ++i) {
        EXPECT_CALL(*protocol, send(testData, 5, TYPE_INT, SEND_TAG, i+1)).Times(Exactly(1));
    }
    for (size_t i = 0; i < main.getEndpoint()->getClient_list().size(); ++i) {
        main.send(testData, 5, TYPE_INT, SEND_TAG, i+1);
    }
}

TEST_F(ServerTest, Destroy) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainServer(main, protocol, num_program);

    ExpectEndpointTypeInquiry(protocol, is_server, num_program-1);
    for (size_t i = 0; i < main.getEndpoint()->getClient_list().size(); ++i) {
        EXPECT_CALL(*protocol, closeConnection(i+1)).Times(Exactly(1));
        EXPECT_CALL(*protocol, destroy(i+1)).Times(Exactly(1));
    }
    main.destroy();
}

class ClientTest : public ServerTest {
  protected:
    int client_id = 4;
    void SetUp() override { is_server = false; }
};

void InitializeMainClient(MCLMain& main, std::shared_ptr<MockTransport>& protocol,
                          int num_program, int client_id) {
    main.setProtocol(protocol);
    EXPECT_CALL(*protocol, initialize(nullptr)).Times(Exactly(1));
    ExpectEndpointTypeInquiry(protocol, false, 1);
    EXPECT_CALL(*protocol, getNPrograms()).Times(Exactly(1)).WillRepeatedly(Return(num_program));
    EXPECT_CALL(*protocol, getProgramId()).Times(Exactly(1)).WillOnce(Return(client_id));
    EXPECT_CALL(*protocol, connect(0)).Times(Exactly(1));
    EXPECT_CALL(*protocol, send(_, 1, TYPE_INT, _, 0)).Times(Exactly(1));
    main.initialize(nullptr);
}

TEST_F(ClientTest, Initialize) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainClient(main, protocol, num_program, client_id);
    
    ASSERT_EQ(client_id, main.getEndpoint()->getId());
    ASSERT_EQ(1u, main.getEndpoint()->getClient_list().size());
    ASSERT_EQ(0, main.getEndpoint()->getClient_list()[0]->getId());
}

TEST_F(ClientTest, Send) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainClient(main, protocol, num_program, client_id);
    
    int testData[5];
    ExpectEndpointTypeInquiry(protocol, is_server, 1);
    EXPECT_CALL(*protocol, send(testData, 5, TYPE_INT, SEND_TAG, 0)).Times(Exactly(1));
    main.send(testData, 5, TYPE_INT, SEND_TAG, 0);
}

TEST_F(ClientTest, Receive) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainClient(main, protocol, num_program, client_id);
    
    int testData[5];
    ExpectEndpointTypeInquiry(protocol, is_server, 1);
    EXPECT_CALL(*protocol, receive(testData, 5, TYPE_INT, RECEIVE_TAG, 0)).Times(Exactly(1));
    main.receive(testData, 5, TYPE_INT, RECEIVE_TAG, 0);
}

TEST_F(ClientTest, Destroy) {
    MCLMain main = MCLMain::getInstance();
    InitializeMainClient(main, protocol, num_program, client_id);

    ExpectEndpointTypeInquiry(protocol, is_server, 1);
    EXPECT_CALL(*protocol, closeConnection(0)).Times(Exactly(1));
    main.destroy();
}

TEST(InitializeFail, LessThan2Programs) {
    MCLMain main = MCLMain::getInstance();
    auto protocol = std::make_shared<MockTransport>();
    main.setProtocol(protocol);

    ON_CALL(*protocol, getNPrograms()).WillByDefault(Return(1));
    std::string error_msg = "A simulation is running with less than two programs.";
    ASSERT_DEATH(main.initialize(nullptr), error_msg);
}

TEST(Uninitialized, Send) {
    MCLMain main = MCLMain::getInstance();
    int buffer;
    std::string error_msg = "It seems that MCL was not initialized properly.";
    ASSERT_DEATH(main.send(&buffer, 1, 0, 0, 0), error_msg);
}

TEST(Uninitialized, Receive) {
    MCLMain main = MCLMain::getInstance();
    int buffer;
    std::string error_msg = "It seems that MCL was not initialized properly.";
    ASSERT_DEATH(main.receive(&buffer, 1, 0, 0, 0), error_msg);
}

TEST(Uninitialized, GetProgramNum) {
    MCLMain main = MCLMain::getInstance();
    std::string error_msg = "It seems that MCL was not initialized properly.";
    ASSERT_DEATH(main.getProgramNum(), error_msg);
}

TEST(Uninitialized, GetProgramId) {
    MCLMain main = MCLMain::getInstance();
    std::string error_msg = "It seems that MCL was not initialized properly.";
    ASSERT_DEATH(main.getProgramId(), error_msg);
}

TEST(Uninitialized, Destroy) {
    MCLMain main = MCLMain::getInstance();
    std::string error_msg = "It seems that MCL was not initialized properly.";
    ASSERT_DEATH(main.destroy(), error_msg);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleMock(&argc, argv);
    auto result = RUN_ALL_TESTS();
    return result;
}
