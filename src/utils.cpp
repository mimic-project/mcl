//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "utils.h"

#include <cstdlib>
#include <cctype>

#include "error_handler.h"

namespace mcl {

int get_env_var_int (const std::string &var_name, int default_val) {
    // if a default value is not specified, the variable is required
    bool required = default_val == -100;

    const auto env_var = get_env_var_str(var_name, required);
    if (represents_integer(env_var)) {
        return std::stoi(env_var);
    } else if (required) {
        handle_error(ERROR_FATAL, "The environment variable, " + std::string(var_name) +
                                  " is not an integer!", EARGS);
    }
    return default_val;
}

std::string get_env_var_str (const std::string &var_name, bool required) {
    const auto env_var = std::getenv(var_name.c_str());
    if (env_var != nullptr) {
        return std::string{env_var};
    } else if (required) {
        handle_error(ERROR_FATAL, "The environment variable, " + std::string(var_name) +
                                  " is not assigned!", EARGS);
    }
    return std::string();
}

bool represents_integer(const std::string &input) {
    if (input.empty()) return false;

    // 1. integral types may be negative
    const auto minus_pos = input.find("-");
    auto iter = input.cbegin();
    if (minus_pos != std::string::npos) {
        if (minus_pos == 0 && input.size() > 1) {
            ++iter;
        } else {
            // either the var is just a string with one char, '-'
            // or it's a string with a minus somewhere
            return false;
        }
    }

    // 2. iterate through the string to find a non-integral character
    for (; iter != input.cend(); ++iter) {
        if (!std::isdigit(static_cast<int>(*iter))) {
            return false;
        }
    }
    return true;
}

} // namespace mcl
